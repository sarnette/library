﻿Imports System
Imports System.ComponentModel
Imports System.Windows.Forms
Imports System.Drawing
Imports MySql.Data.MySqlClient

Module Form
    Public Class Form1
        Inherits Windows.Forms.Form

        'All items that are present in the menu bar of the form.
        Private LibraryMenu As MenuStrip
        Private FileMenu As ToolStripMenuItem
        Private WithEvents CheckoutSubMenu As ToolStripMenuItem
        Private WithEvents CheckinSubMenu As ToolStripMenuItem
        Private WithEvents ExitSubMenu As ToolStripMenuItem
        Private CustomerMenu As ToolStripMenuItem
        Private WithEvents ViewCustomerSubMenu As ToolStripMenuItem
        Private WithEvents NewCustomerSubMenu As ToolStripMenuItem
        Private WithEvents UpdateCustomerSubMenu As ToolStripMenuItem
        Private ReportsMenu As ToolStripMenuItem
        Private WithEvents AllBooksCheckedOutSubMenu As ToolStripMenuItem
        Private WithEvents BooksCheckedOutByCustomerSubMenu As ToolStripMenuItem
        Private WithEvents LateBooksSubMenu As ToolStripMenuItem
        Private WithEvents UpdateConfigMenu As ToolStripMenuItem

        'All items that are present in the panel for checking books out.
        Private CheckoutPanel As Panel
        Private CheckoutLabel As Label
        Private CheckoutCustomerCombobox As ComboBox
        Private CheckoutBooksCombobox As ComboBox
        Private WithEvents CheckoutButton As Button
        Private WithEvents CheckoutCloseButton As Button

        'All items that are present in the panel for checking books in.
        Private CheckinPanel As Panel
        Private CheckinLabel As Label
        Private WithEvents CheckinCustomerCombobox As ComboBox
        Private CheckinBookCombobox As ComboBox
        Private WithEvents CheckinButton As Button
        Private WithEvents CheckinCloseButton As Button

        'All items that are present in the panel for viewing customers.
        Private ViewCustomerPanel As Panel
        Private ViewCustomerLabel As Label
        Private ViewCustomerDataSource As DataGridView
        Private WithEvents ViewCustomerCloseButton As Button

        'All items that are present in the panel for adding new customers.
        Private NewCustomerPanel As Panel
        Private NewCustomerLabel As Label
        Private NewFirstNameTextbox As TextBox
        Private NewLastNameTextbox As TextBox
        Private NewCustomerDateTimePicker As DateTimePicker
        Private NewCustomerCheckbox As CheckBox
        Private WithEvents AddCustomerButton As Button
        Private WithEvents CloseNewCustomerPanelButton As Button

        'All items that are present in the panel for updating existing customers.
        Private UpdateCustomerPanel As Panel
        Private UpdateCustomerLabel As Label
        Private WithEvents UpdateCustomerIdCombobox As ComboBox
        Private UpdateFirstNameTextbox As TextBox
        Private UpdateLastNameTextbox As TextBox
        Private UpdateCustomerDateTimePicker As DateTimePicker
        Private UpdateCustomerCheckbox As CheckBox
        Private WithEvents UpdateCustomerButton As Button
        Private WithEvents CloseUpdateCustomerPanelButton As Button

        'All items that are present in the panel for viewing all books currently checked out.
        Private AllBooksCheckedOutPanel As Panel
        Private AllBooksCheckedOutLabel As Label
        Private AllBooksCheckedOutDataSource As DataGridView
        Private WithEvents AllBooksCheckedOutCloseButton As Button

        'All items that are present in the panel for viewing books checked out by customer.
        Private BooksCheckedOutByCustomerPanel As Panel
        Private BooksCheckedOutByCustomerLabel As Label
        Private BooksCheckedOutByCustomerDataSource As DataGridView
        Private BooksCheckedOutByCustomerCombobox As ComboBox
        Private WithEvents BooksCheckedOutByCustomerGenerateButton As Button
        Private WithEvents BooksCheckedOutByCustomerCloseButton As Button

        'All items that are present in the panel for viewing all late books.
        Private LateBooksReportPanel As Panel
        Private LateBooksReportLabel As Label
        Private LateBooksReportDataSource As DataGridView
        Private WithEvents LateBooksReportCloseButton As Button

        'All items that are present in the panel for updating database configuration.
        Private UpdateConfigPanel As Panel
        Private UpdateConfigLabel As Label
        Private WithEvents UpdateConfigCombobox As ComboBox
        Private UpdateConfigCheckoutTextbox As TextBox
        Private WithEvents UpdateConfigCheckoutButton As Button
        Private UpdateConfigLateFeeTextbox As TextBox
        Private WithEvents UpdateConfigLateFeeButton As Button
        Private WithEvents UpdateConfigCloseButton As Button

        'Button helper label that displays status messages.
        Private WithEvents StatusLabel As Label

        Public Sub New()
            CreateMenu()
            CreateCheckoutPanel()
            CreateCheckinPanel()
            CreateViewCustomerPanel()
            CreateNewCustomerPanel()
            CreateUpdateCustomerPanel()
            CreateAllBooksCheckedOutPanel()
            CreateBooksCheckedOutByCustomerPanel()
            CreateLateBooksReportPanel()
            CreateUpdateConfigPanel()
            CreateStatusLabel()

            'Adds the menu bar to the form.
            Me.Controls.Add(LibraryMenu)
            Me.MainMenuStrip = LibraryMenu

            'Adds the panel for checking out books.
            Me.Controls.Add(CheckoutPanel)
            CheckoutPanel.Controls.AddRange({CheckoutLabel, CheckoutCustomerCombobox, CheckoutBooksCombobox, CheckoutButton, CheckoutCloseButton})

            'Adds the panel for checking books in.
            Me.Controls.Add(CheckinPanel)
            CheckinPanel.Controls.AddRange({CheckinLabel, CheckinCustomerCombobox, CheckinBookCombobox, CheckinButton, CheckinCloseButton})

            'Adds the panel for viewing customers and all related controls to that panel to the form.
            Me.Controls.Add(ViewCustomerPanel)
            ViewCustomerPanel.Controls.AddRange({ViewCustomerLabel, ViewCustomerDataSource, ViewCustomerCloseButton})

            'Adds the panel for creating new customers and all related controls to that panel to the form.
            Me.Controls.Add(NewCustomerPanel)
            NewCustomerPanel.Controls.AddRange({NewCustomerLabel, NewFirstNameTextbox, NewLastNameTextbox, NewCustomerDateTimePicker, NewCustomerCheckbox, AddCustomerButton, CloseNewCustomerPanelButton})

            'Adds the panel for updating existing customers and all related controls to that panel to the form.
            Me.Controls.Add(UpdateCustomerPanel)
            UpdateCustomerPanel.Controls.AddRange({UpdateCustomerLabel, UpdateCustomerIdCombobox, UpdateFirstNameTextbox, UpdateLastNameTextbox, UpdateCustomerDateTimePicker, UpdateCustomerCheckbox, UpdateCustomerButton, CloseUpdateCustomerPanelButton})

            'Adds the panel for viewing all books checked out and all related controls to that panel to the form.
            Me.Controls.Add(AllBooksCheckedOutPanel)
            AllBooksCheckedOutPanel.Controls.AddRange({AllBooksCheckedOutLabel, AllBooksCheckedOutDataSource, AllBooksCheckedOutCloseButton})

            'Adds the panel for viewing books checked out by customer and all related controls to that panel to the form.
            Me.Controls.Add(BooksCheckedOutByCustomerPanel)
            BooksCheckedOutByCustomerPanel.Controls.AddRange({BooksCheckedOutByCustomerLabel, BooksCheckedOutByCustomerDataSource, BooksCheckedOutByCustomerCombobox, BooksCheckedOutByCustomerGenerateButton, BooksCheckedOutByCustomerCloseButton})

            'Adds the panel for viewing all late books and all related controls to that panel to the form.
            Me.Controls.Add(LateBooksReportPanel)
            LateBooksReportPanel.Controls.AddRange({LateBooksReportLabel, LateBooksReportDataSource, LateBooksReportCloseButton})

            'Adds the panel for updating basic configuration.
            Me.Controls.Add(UpdateConfigPanel)
            UpdateConfigPanel.Controls.AddRange({UpdateConfigLabel, UpdateConfigCombobox, UpdateConfigCheckoutTextbox, UpdateConfigCheckoutButton, UpdateConfigLateFeeTextbox, UpdateConfigLateFeeButton, UpdateConfigCloseButton})

            'Adds the status label.
            Me.Controls.Add(StatusLabel)

            'Sets information about the form.
            Me.Name = "Library"
            Me.Text = "Library"
            Me.StatusLabel.BringToFront()
            Me.WindowState = FormWindowState.Maximized
        End Sub

        'Subroutine that creates the menu bar object(s) for the form.
        Private Sub CreateMenu()
            FileMenu = New ToolStripMenuItem With {
                .Text = "File"
                }
            CheckoutSubMenu = New ToolStripMenuItem With {
                .Text = "New Book Checkout"
            }
            CheckinSubMenu = New ToolStripMenuItem With {
                .Text = "New Book Check In"
            }
            ExitSubMenu = New ToolStripMenuItem With {
                .Text = "Exit"
            }
            FileMenu.DropDownItems.AddRange({CheckoutSubMenu, CheckinSubMenu, ExitSubMenu})

            CustomerMenu = New ToolStripMenuItem With {
                .Text = "Customer"
            }
            ViewCustomerSubMenu = New ToolStripMenuItem With {
                .Text = "View Customers"
            }
            NewCustomerSubMenu = New ToolStripMenuItem With {
                .Text = "Add New Customer"
            }
            UpdateCustomerSubMenu = New ToolStripMenuItem With {
                .Text = "Edit Existing Customer"
            }
            CustomerMenu.DropDownItems.AddRange({ViewCustomerSubMenu, NewCustomerSubMenu, UpdateCustomerSubMenu})

            ReportsMenu = New ToolStripMenuItem With {
                .Text = "Reports"
            }
            AllBooksCheckedOutSubMenu = New ToolStripMenuItem With {
                .Text = "All Checked Out Report"
            }
            BooksCheckedOutByCustomerSubMenu = New ToolStripMenuItem With {
                .Text = "Checked Out By Customer Report"
            }
            LateBooksSubMenu = New ToolStripMenuItem With {
                .Text = "Late Books"
            }
            ReportsMenu.DropDownItems.AddRange({AllBooksCheckedOutSubMenu, BooksCheckedOutByCustomerSubMenu, LateBooksSubMenu})

            UpdateConfigMenu = New ToolStripMenuItem With {
                .Text = "Config"
            }

            LibraryMenu = New MenuStrip With {
                .Dock = DockStyle.Top
            }
            LibraryMenu.Items.AddRange({FileMenu, CustomerMenu, ReportsMenu, UpdateConfigMenu})
        End Sub

        'Subroutine that creates the controls for checking out books.
        Private Sub CreateCheckoutPanel()
            CheckoutPanel = New Panel With {
                .Location = New Point(5, 30),
                .AutoSize = True,
                .Anchor = AnchorStyles.Top Or AnchorStyles.Left Or AnchorStyles.Right Or AnchorStyles.Bottom,
                .BorderStyle = BorderStyle.FixedSingle,
                .Visible = False
            }
            CheckoutLabel = New Label With {
                .Text = "Checkout Book",
                .Font = New Font("checkoutFont", 32, FontStyle.Bold),
                .AutoSize = True,
                .Location = New Point(0, 0)
            }
            CheckoutCustomerCombobox = New ComboBox With {
                .Size = New Size(100, 50),
                .Location = New Point(0, 100),
                .DropDownStyle = ComboBoxStyle.DropDownList
            }
            CheckoutBooksCombobox = New ComboBox With {
                .Size = New Size(100, 50),
                .Location = New Point(150, 100),
                .DropDownStyle = ComboBoxStyle.DropDownList
            }
            CheckoutButton = New Button With {
                .Size = New Size(100, 50),
                .Location = New Point(0, 200),
                .Text = "Checkout"
            }
            CheckoutCloseButton = New Button With {
                .Size = New Size(100, 50),
                .Location = New Point(150, 200),
                .Text = "Close"
            }
        End Sub

        'Subroutine that creates the controls for checking books in.
        Private Sub CreateCheckinPanel()
            CheckinPanel = New Panel With {
                .Location = New Point(5, 30),
                .AutoSize = True,
                .Anchor = AnchorStyles.Top Or AnchorStyles.Left Or AnchorStyles.Right Or AnchorStyles.Bottom,
                .BorderStyle = BorderStyle.FixedSingle,
                .Visible = False
            }
            CheckinLabel = New Label With {
                .Text = "Check In Book",
                .Font = New Font("checkinFont", 32, FontStyle.Bold),
                .AutoSize = True,
                .Location = New Point(0, 0)
            }
            CheckinCustomerCombobox = New ComboBox With {
                .Size = New Size(100, 50),
                .Location = New Point(0, 100),
                .DropDownStyle = ComboBoxStyle.DropDownList
            }
            CheckinBookCombobox = New ComboBox With {
                .Size = New Size(100, 50),
                .Location = New Point(150, 100),
                .DropDownStyle = ComboBoxStyle.DropDownList
            }
            CheckinButton = New Button With {
                .Size = New Size(100, 50),
                .Location = New Point(0, 200),
                .Text = "Check In Book"
            }
            CheckinCloseButton = New Button With {
                .Size = New Size(100, 50),
                .Location = New Point(150, 200),
                .Text = "Close"
            }
        End Sub

        'Subroutine that creates the controls for viewing all customers.
        Private Sub CreateViewCustomerPanel()
            ViewCustomerPanel = New Panel With {
                .Location = New Point(5, 30),
                .AutoSize = True,
                .Anchor = AnchorStyles.Top Or AnchorStyles.Left Or AnchorStyles.Right Or AnchorStyles.Bottom,
                .BorderStyle = BorderStyle.FixedSingle,
                .Visible = False
            }
            ViewCustomerLabel = New Label With {
                .Text = "View Customers",
                .Font = New Font("viewCustomerFont", 32, FontStyle.Bold),
                .AutoSize = True,
                .Location = New Point(0, 0)
            }
            ViewCustomerDataSource = New DataGridView With {
                .ReadOnly = True,
                .Size = New Size(500, 300),
                .Location = New Point(0, 100),
                .AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells
            }
            ViewCustomerCloseButton = New Button With {
                .Size = New Size(100, 50),
                .Location = New Point(0, 450),
                .Text = "Close"
            }
        End Sub

        'Subroutine that creates the controls for adding new customers.
        Private Sub CreateNewCustomerPanel()
            NewCustomerPanel = New Panel With {
                .Location = New Point(5, 30),
                .AutoSize = True,
                .Anchor = AnchorStyles.Top Or AnchorStyles.Left Or AnchorStyles.Right Or AnchorStyles.Bottom,
                .BorderStyle = BorderStyle.FixedSingle,
                .Visible = False
                }
            NewCustomerLabel = New Label With {
                .Text = "New Customer",
                .Font = New Font("newCustomerFont", 32, FontStyle.Bold),
                .AutoSize = True,
                .Location = New Point(0, 0)
                }
            NewFirstNameTextbox = New TextBox With {
                .Size = New Size(100, 50),
                .Location = New Point(0, 100),
                .Text = "First Name"
                }
            NewLastNameTextbox = New TextBox With {
                .Size = New Size(100, 50),
                .Location = New Point(150, 100),
                .Text = "Last Name"
                }
            NewCustomerDateTimePicker = New DateTimePicker With {
                .Location = New Point(0, 200)
                }
            NewCustomerCheckbox = New CheckBox With {
                .Location = New Point(0, 250),
                .Text = "Is teacher?"
                }
            AddCustomerButton = New Button With {
                .Size = New Size(100, 50),
                .Location = New Point(0, 350),
                .Text = "Add Customer"
                }
            CloseNewCustomerPanelButton = New Button With {
                .Size = New Size(100, 50),
                .Location = New Point(150, 350),
                .Text = "Close"
                }
        End Sub

        'Subroutine that creates the controls for editing existing customers.
        Private Sub CreateUpdateCustomerPanel()
            UpdateCustomerPanel = New Panel With {
                .Location = New Point(5, 30),
                .AutoSize = True,
                .Anchor = AnchorStyles.Top Or AnchorStyles.Left Or AnchorStyles.Right Or AnchorStyles.Bottom,
                .BorderStyle = BorderStyle.FixedSingle,
                .Visible = False
            }
            UpdateCustomerLabel = New Label With {
                .Text = "Update Customer",
                .Font = New Font("updateCustomerFont", 32, FontStyle.Bold),
                .AutoSize = True,
                .Location = New Point(0, 0)
            }
            UpdateCustomerIdCombobox = New ComboBox With {
                .Size = New Size(100, 50),
                .Location = New Point(0, 100),
                .DropDownStyle = ComboBoxStyle.DropDownList
            }
            UpdateFirstNameTextbox = New TextBox With {
                .Size = New Size(100, 50),
                .Location = New Point(0, 200),
                .Text = "First Name"
            }
            UpdateLastNameTextbox = New TextBox With {
                .Size = New Size(100, 50),
                .Location = New Point(150, 200),
                .Text = "Last Name"
            }
            UpdateCustomerDateTimePicker = New DateTimePicker With {
                .Location = New Point(0, 300)
            }
            UpdateCustomerCheckbox = New CheckBox With {
                .Location = New Point(0, 350),
                .Text = "Is teacher?"
            }
            UpdateCustomerButton = New Button With {
                .Size = New Size(100, 50),
                .Location = New Point(0, 400),
                .Text = "Update Customer"
            }
            CloseUpdateCustomerPanelButton = New Button With {
                .Size = New Size(100, 50),
                .Location = New Point(150, 400),
                .Text = "Close"
            }
        End Sub

        'Subroutine that creates the controls for viewing all checked out books.
        Private Sub CreateAllBooksCheckedOutPanel()
            AllBooksCheckedOutPanel = New Panel With {
                .Location = New Point(5, 30),
                .AutoSize = True,
                .Anchor = AnchorStyles.Top Or AnchorStyles.Left Or AnchorStyles.Right Or AnchorStyles.Bottom,
                .BorderStyle = BorderStyle.FixedSingle,
                .Visible = False
            }
            AllBooksCheckedOutLabel = New Label With {
                .Text = "All Books Checked Out",
                .Font = New Font("allBooksCheckedOutFont", 32, FontStyle.Bold),
                .AutoSize = True,
                .Location = New Point(0, 0)
            }
            AllBooksCheckedOutDataSource = New DataGridView With {
                .Size = New Size(500, 300),
                .AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells,
                .Location = New Point(0, 100)
            }
            AllBooksCheckedOutCloseButton = New Button With {
                .Size = New Size(100, 50),
                .Location = New Point(0, 450),
                .Text = "Close Report"
                }
        End Sub

        'Subroutine that creates the controls for viewing checked out books per customer.
        Private Sub CreateBooksCheckedOutByCustomerPanel()
            BooksCheckedOutByCustomerPanel = New Panel With {
                .Location = New Point(5, 30),
                .AutoSize = True,
                .Anchor = AnchorStyles.Top Or AnchorStyles.Left Or AnchorStyles.Right Or AnchorStyles.Bottom,
                .BorderStyle = BorderStyle.FixedSingle,
                .Visible = False
            }
            BooksCheckedOutByCustomerLabel = New Label With {
                .Text = "Books Out By Customer",
                .Font = New Font("allBooksCheckedOutFont", 32, FontStyle.Bold),
                .AutoSize = True,
                .Location = New Point(0, 0)
            }
            BooksCheckedOutByCustomerDataSource = New DataGridView With {
                .Size = New Size(500, 300),
                .AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells,
                .Location = New Point(0, 100),
                .ReadOnly = True
            }
            BooksCheckedOutByCustomerCombobox = New ComboBox With {
                .Size = New Drawing.Size(100, 80),
                .Location = New Point(0, 400),
                .DropDownStyle = ComboBoxStyle.DropDownList
            }
            BooksCheckedOutByCustomerGenerateButton = New Button With {
                .Size = New Drawing.Size(100, 50),
                .Location = New Point(0, 450),
                .Text = "Generate Report"
            }
            BooksCheckedOutByCustomerCloseButton = New Button With {
                .Size = New Drawing.Size(100, 50),
                .Location = New Point(150, 450),
                .Text = "Close Report"
            }
        End Sub

        'Subroutine that creates the controls for viewing all late books.
        Private Sub CreateLateBooksReportPanel()
            LateBooksReportPanel = New Panel With {
                .Location = New Point(5, 30),
                .AutoSize = True,
                .Anchor = AnchorStyles.Top Or AnchorStyles.Left Or AnchorStyles.Right Or AnchorStyles.Bottom,
                .BorderStyle = BorderStyle.FixedSingle,
                .Visible = False
            }
            LateBooksReportLabel = New Label With {
                .Text = "All Late Books",
                .Font = New Font("lateBooksReportFont", 32, FontStyle.Bold),
                .AutoSize = True,
                .Location = New Point(0, 0)
            }
            LateBooksReportDataSource = New DataGridView With {
                .Size = New Size(500, 300),
                .Location = New Point(0, 100),
                .AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells
            }
            LateBooksReportCloseButton = New Button With {
                .Size = New Size(100, 50),
                .Location = New Point(0, 450),
                .Text = "Close Report"
                }
        End Sub

        'Subroutine that creates the controls for updating checkout limit and late fee values.
        Private Sub CreateUpdateConfigPanel()
            Dim comboboxDataTable As New DataTable
            comboboxDataTable.Columns.Add("Text", GetType(String))
            comboboxDataTable.Columns.Add("Value", GetType(Int32))
            comboboxDataTable.Rows.Add("Student", 1)
            comboboxDataTable.Rows.Add("Teacher", 2)

            UpdateConfigPanel = New Panel With {
                .Location = New Point(5, 30),
                .AutoSize = True,
                .Anchor = AnchorStyles.Top Or AnchorStyles.Left Or AnchorStyles.Right Or AnchorStyles.Bottom,
                .BorderStyle = BorderStyle.FixedSingle,
                .Visible = False
            }
            UpdateConfigLabel = New Label With {
                .Text = "Update Config",
                .Font = New Font("updateConfigFont", 32, FontStyle.Bold),
                .AutoSize = True,
                .Location = New Point(0, 0)
            }
            UpdateConfigCombobox = New ComboBox With {
                .Size = New Size(100, 50),
                .Location = New Point(0, 100),
                .DropDownStyle = ComboBoxStyle.DropDownList,
                .DataSource = comboboxDataTable,
                .DisplayMember = "Text",
                .ValueMember = "Value"
            }
            UpdateConfigCheckoutTextbox = New TextBox With {
                .Size = New Size(100, 50),
                .Location = New Point(0, 150),
                .Text = "Checkout Limit"
            }
            UpdateConfigCheckoutButton = New Button With {
                .Size = New Size(100, 50),
                .Location = New Point(150, 150),
                .Text = "Update Checkout Limit"
            }
            UpdateConfigLateFeeTextbox = New TextBox With {
                .Size = New Size(100, 50),
                .Location = New Point(0, 250),
                .Text = "Late Fee"
            }
            UpdateConfigLateFeeButton = New Button With {
                .Size = New Size(100, 50),
                .Location = New Point(150, 250),
                .Text = "Update Late Fee"
            }
            UpdateConfigCloseButton = New Button With {
                .Size = New Size(100, 50),
                .Location = New Point(0, 350),
                .Text = "Close"
            }
        End Sub

        'Subroutine that creates the status label for displaying status.
        Private Sub CreateStatusLabel()
            StatusLabel = New Label With {
                .Text = "Status",
                .Size = New Size(500, 25),
                .Anchor = AnchorStyles.Bottom,
                .Dock = DockStyle.Bottom
            }
        End Sub

        'Subroutine that runs when the form is created. Creates the database connection string and tests a connection.
        Private Sub FormLoad(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
            msdConnection.BuildConnectionString()
            Me.AutoSize = True
        End Sub

        'Subroutine called when the checkout menu button is clicked. Hides all panels but checkout.
        Private Sub CheckoutSubMenu_click(ByVal sender As Object, ByVal e As EventArgs) Handles CheckoutSubMenu.Click
            For Each controlItem As Control In Me.Controls
                If TypeOf (controlItem) Is Panel Then
                    controlItem.Visible = False
                End If
            Next
            CheckoutPanel.Visible = True

            Dim customerQuery = "SELECT last_name, customerId FROM customer"
            Dim customerDataTable As New DataTable
            Try
                ConnectDatabase()
                Dim customerDataAdapter As New MySqlDataAdapter(customerQuery, msdConnection.connection)
                customerDataAdapter.Fill(customerDataTable)
                CheckoutCustomerCombobox.DataSource = customerDataTable
                CheckoutCustomerCombobox.DisplayMember = "last_name"
                CheckoutCustomerCombobox.ValueMember = "customerId"
                UpdateBooksForCheckout()

                StatusLabel.Text = "Checkout information loaded."
            Catch ex As Exception
                Console.WriteLine("Query failed: " + ex.ToString)
                StatusLabel.Text = "Database connection error."
            Finally
                DisconnectDatabase()
            End Try
        End Sub

        'Subroutine that loads books available for checkout.
        Private Sub UpdateBooksForCheckout()
            Dim bookQuery = "SELECT books.Book_Id, books.Book_title FROM books WHERE NOT EXISTS(SELECT * FROM checkout WHERE books.book_id = checkout.bookid AND checkout.Date_Returned IS NULL)"
            Dim bookDataTable As New DataTable
            Using bookDataAdapter As New MySqlDataAdapter(bookQuery, msdConnection.connection)
                Try
                    ConnectDatabase()
                    bookDataAdapter.Fill(bookDataTable)
                    CheckoutBooksCombobox.DataSource = bookDataTable
                    CheckoutBooksCombobox.DisplayMember = "Book_title"
                    CheckoutBooksCombobox.ValueMember = "Book_Id"
                Catch ex As Exception
                    Console.WriteLine("Query failed: " + ex.ToString)
                Finally
                    DisconnectDatabase()
                End Try
            End Using
        End Sub

        'Subroutine called when the checkin menu button is clicked. Hides all panels but checkin.
        Private Sub CheckinSubMenu_click(ByVal sender As Object, ByVal e As EventArgs) Handles CheckinSubMenu.Click
            For Each controlItem As Control In Me.Controls
                If TypeOf (controlItem) Is Panel Then
                    controlItem.Visible = False
                End If
            Next
            CheckinPanel.Visible = True

            Dim customerQuery = "SELECT DISTINCT customer.last_name, customer.customerId FROM (checkout INNER JOIN customer ON checkout.custId = customer.customerId) WHERE Date_Returned IS NULL"
            Dim customerDataTable As New DataTable
            Try
                ConnectDatabase()
                Dim customerDataAdapter As New MySqlDataAdapter(customerQuery, msdConnection.connection)
                customerDataAdapter.Fill(customerDataTable)
                CheckinCustomerCombobox.DataSource = customerDataTable
                CheckinCustomerCombobox.DisplayMember = "last_name"
                CheckinCustomerCombobox.ValueMember = "customerId"
                StatusLabel.Text = "Checkin information loaded."
            Catch ex As Exception
                Console.WriteLine("Query failed: " + ex.ToString)
                StatusLabel.Text = "Database connection error."
            Finally
                DisconnectDatabase()
            End Try
        End Sub

        'Subroutine called when the exit menu button is clicked. Closes the application.
        Private Sub ExitSubMenu_click(ByVal sender As Object, ByVal e As EventArgs) Handles ExitSubMenu.Click
            End
        End Sub

        'Subroutine called when the view customers menu button is clicked. Hides all panels but the view customer.
        Private Sub ViewCustomerSubMenu_click(ByVal sender As Object, ByVal e As EventArgs) Handles ViewCustomerSubMenu.Click
            For Each controlItem As Control In Me.Controls
                If TypeOf (controlItem) Is Panel Then
                    controlItem.Visible = False
                End If
            Next
            ViewCustomerPanel.Visible = True

            Dim query As String = "SELECT * FROM customer;"
            Dim dataTable As New DataTable
            Try
                ConnectDatabase()
                Dim dataAdapter As New MySqlDataAdapter(query, msdConnection.connection)
                dataAdapter.Fill(dataTable)
                ViewCustomerDataSource.DataSource = dataTable
                StatusLabel.Text = "Customer data loaded."
            Catch ex As Exception
                Console.WriteLine("Query failed: " + ex.ToString)
                StatusLabel.Text = "Database connection error."
            Finally
                DisconnectDatabase()
            End Try
        End Sub

        'Subroutine called when the new customer menu button is clicked. Hides all panels but the new customer. 
        Private Sub NewCustomerSubMenu_click(ByVal sender As Object, ByVal e As EventArgs) Handles NewCustomerSubMenu.Click
            For Each controlItem As Control In Me.Controls
                If TypeOf (controlItem) Is Panel Then
                    controlItem.Visible = False
                End If
            Next
            NewCustomerPanel.Visible = True
        End Sub

        'Subroutine called when the update customer menu button is clicked. Hides all panels but the update customer.
        Private Sub UpdateCustomerSubMenu_click(ByVal sender As Object, ByVal e As EventArgs) Handles UpdateCustomerSubMenu.Click
            For Each controlItem As Control In Me.Controls
                If TypeOf (controlItem) Is Panel Then
                    controlItem.Visible = False
                End If
            Next
            UpdateCustomerPanel.Visible = True

            Dim query As String = "SELECT customerId FROM customer"
            Dim dataTable As New DataTable
            Try
                ConnectDatabase()
                Dim dataAdapter As New MySqlDataAdapter(query, msdConnection.connection)
                dataAdapter.Fill(dataTable)
                UpdateCustomerIdCombobox.DataSource = dataTable
                UpdateCustomerIdCombobox.DisplayMember = "customerId"
                UpdateCustomerIdCombobox.ValueMember = "customerId"
                StatusLabel.Text = "Customer data loaded."
            Catch ex As Exception
                Console.WriteLine("Query failed: " + ex.ToString)
                StatusLabel.Text = "Database connection error."
            Finally
                DisconnectDatabase()
            End Try
        End Sub

        'Subroutine called when the all checked out books menu button is clicked. Hides all panels but the all checked out books. 
        Private Sub AllBooksCheckedOutSubMenu_click(ByVal sender As Object, ByVal e As EventArgs) Handles AllBooksCheckedOutSubMenu.Click
            For Each controlItem As Control In Me.Controls
                If TypeOf (controlItem) Is Panel Then
                    controlItem.Visible = False
                End If
            Next
            AllBooksCheckedOutPanel.Visible = True

            Dim query As String = "SELECT books.Book_title, books.Author FROM ((checkout INNER JOIN customer on checkout.custId = customer.customerId) INNER JOIN books on checkout.bookId = books.Book_id) WHERE checkout.Date_Returned IS NULL"
            Dim dataTable As New DataTable
            Try
                ConnectDatabase()
                Dim dataAdapter As New MySqlDataAdapter(query, msdConnection.connection)
                dataAdapter.Fill(dataTable)
                AllBooksCheckedOutDataSource.DataSource = dataTable
                StatusLabel.Text = "Report loaded: All books currently checked out."
            Catch ex As Exception
                Console.WriteLine("Query failed: " + ex.ToString)
                StatusLabel.Text = "Database connection error."
            Finally
                DisconnectDatabase()
            End Try
        End Sub

        'Subroutine called when the books out by customer menu button is clicked. Hides all panels but the books checked out by customer. 
        Private Sub BooksCheckedOutByCustomerSubMenu_click(ByVal sender As Object, ByVal e As EventArgs) Handles BooksCheckedOutByCustomerSubMenu.Click
            For Each controlItem As Control In Me.Controls
                If TypeOf (controlItem) Is Panel Then
                    controlItem.Visible = False
                End If
            Next
            BooksCheckedOutByCustomerPanel.Visible = True

            Dim query As String = "SELECT DISTINCT custId FROM checkout WHERE Date_Returned IS NULL"
            Dim dataTable As New DataTable
            Try
                ConnectDatabase()
                Dim dataAdapter As New MySqlDataAdapter(query, msdConnection.connection)
                dataAdapter.Fill(dataTable)
                BooksCheckedOutByCustomerCombobox.DataSource = dataTable
                BooksCheckedOutByCustomerCombobox.DisplayMember = "custId"
                BooksCheckedOutByCustomerCombobox.ValueMember = "custId"
                StatusLabel.Text = "Customers with checkouts loaded."
            Catch ex As Exception
                Console.WriteLine("Query failed: " + ex.ToString)
                StatusLabel.Text = "Database connection error."
            Finally
                DisconnectDatabase()
            End Try
        End Sub

        'Subroutine called when late books menu button is clicked. Hides all panels but late books.
        Private Sub LateBooksSubMenu_click(ByVal sender As Object, ByVal e As EventArgs) Handles LateBooksSubMenu.Click
            For Each controlItem As Control In Me.Controls
                If TypeOf (controlItem) Is Panel Then
                    controlItem.Visible = False
                End If
            Next
            LateBooksReportPanel.Visible = True

            Dim query As String = "SELECT customer.first_name, customer.last_name, books.Book_title, books.Author, checkout.Date_Due_Back FROM ((checkout INNER JOIN customer on checkout.custId = customer.customerId) INNER JOIN books on checkout.bookId = books.Book_id) WHERE checkout.Date_Due_Back < CURDATE() AND checkout.Date_Returned IS NULL"
            Try
                ConnectDatabase()
                Dim dataTable As New DataTable
                Dim dataAdapter As New MySqlDataAdapter(query, msdConnection.connection)
                dataAdapter.Fill(dataTable)
                LateBooksReportDataSource.DataSource = dataTable
                StatusLabel.Text = "Report loaded: All checked out books that are late."
            Catch ex As Exception
            Finally
                DisconnectDatabase()
            End Try
        End Sub

        'Subroutine called when the config menu button is clicked. Hides all panels but the update config.
        Private Sub UpdateConfigMenu_click(ByVal sender As Object, ByVal e As EventArgs) Handles UpdateConfigMenu.Click
            For Each controlItem As Control In Me.Controls
                If TypeOf (controlItem) Is Panel Then
                    controlItem.Visible = False
                End If
            Next
            UpdateConfigPanel.Visible = True
        End Sub

        'Subroutine called when the checkout button is clicked in the checkout panel.
        Private Sub CheckoutButton_click(ByVal sender As Object, ByVal e As EventArgs) Handles CheckoutButton.Click
            Dim query = "INSERT INTO checkout(Date_Borrowed, Date_Due_Back, custId, bookId) VALUES (CURDATE(), @Date_Due, @CustID, @BookId)"
            Dim daysQuery = "SELECT config.Numdays FROM (config INNER JOIN customer ON config.CategID = customer.Teacher) WHERE customer.customerId = @CustID"
            Dim booksAllowedQuery = "SELECT config.Numbooks FROM (config INNER JOIN customer ON config.CategID = customer.Teacher) WHERE customer.customerId = @CustID"
            Dim currentBooksOutQuery = "SELECT COUNT(*) FROM checkout WHERE checkout.custId = @CustID AND Date_Returned IS NULL"
            Dim daysAllowed As Double = 0
            Dim booksAllowed As Int32 = 0
            Dim currentBooksOut As Int32 = 0
            Using command As New MySqlCommand(query, msdConnection.connection)
                Try
                    ConnectDatabase()
                    'Reads the number of books allowed by a customer.
                    Dim booksAllowedCommand As New MySqlCommand(booksAllowedQuery, msdConnection.connection)
                    booksAllowedCommand.Parameters.AddWithValue("@CustID", CheckoutCustomerCombobox.SelectedValue)
                    booksAllowedCommand.Prepare()
                    Dim booksAllowedReader As MySqlDataReader = booksAllowedCommand.ExecuteReader()
                    While booksAllowedReader.Read
                        booksAllowed = booksAllowedReader.GetInt32(0)
                    End While
                    booksAllowedCommand.Dispose()

                    'Reads the current number of books out by a customer.
                    Dim booksCurrentOutCommand As New MySqlCommand(currentBooksOutQuery, msdConnection.connection)
                    booksCurrentOutCommand.Parameters.AddWithValue("@CustID", CheckoutCustomerCombobox.SelectedValue)
                    booksCurrentOutCommand.Prepare()
                    Dim booksCurrentOutReader As MySqlDataReader = booksCurrentOutCommand.ExecuteReader()
                    While booksCurrentOutReader.Read
                        currentBooksOut = booksCurrentOutReader.GetInt32(0)
                    End While
                    booksCurrentOutCommand.Dispose()

                    'Checks if the customer can checkout a book. Displays error and ends sub if limit is reached.
                    If (currentBooksOut >= booksAllowed) Then
                        MessageBox.Show("Customer has reached their checkout limit and cannot checkout more books.")
                        StatusLabel.Text = "Customer book limit reached."
                        Return
                    End If

                    'Reads the number of days allowed for a checkout.
                    Dim daysAllowedCommand As New MySqlCommand(daysQuery, msdConnection.connection)
                    daysAllowedCommand.Parameters.AddWithValue("@CustID", CheckoutCustomerCombobox.SelectedValue)
                    daysAllowedCommand.Prepare()
                    Dim daysAllowedReader As MySqlDataReader = daysAllowedCommand.ExecuteReader()
                    While daysAllowedReader.Read
                        daysAllowed = daysAllowedReader.GetDouble(0)
                    End While
                    daysAllowedCommand.Dispose()

                    'Creates the checkout entry
                    command.Parameters.AddWithValue("@Date_Due", Today.AddDays(daysAllowed))
                    command.Parameters.AddWithValue("@CustID", CheckoutCustomerCombobox.SelectedValue)
                    command.Parameters.AddWithValue("@BookID", CheckoutBooksCombobox.SelectedValue)
                    command.Prepare()
                    command.ExecuteNonQuery()

                    UpdateBooksForCheckout()
                    StatusLabel.Text = "Book checked out."
                Catch ex As Exception
                    Console.WriteLine("Query failed: " + ex.ToString)
                    StatusLabel.Text = "Database connection error."
                Finally
                    DisconnectDatabase()
                End Try
            End Using
        End Sub

        'Subroutine called when the close button is clicked in the checkout panel. Hides the panel.
        Private Sub CheckoutCloseButton_click(ByVal sender As Object, ByVal e As EventArgs) Handles CheckoutCloseButton.Click
            CheckoutPanel.Visible = False
        End Sub

        'Subroutine called when a new selection is made on the customer combobox on checkin panel.
        Private Sub CheckinCustomerCombobox_click(ByVal sender As Object, ByVal e As EventArgs) Handles CheckinCustomerCombobox.SelectedValueChanged
            LoadBooksCheckedOut()
        End Sub

        'Subroutine called to load books out by selected customer.
        Private Sub LoadBooksCheckedOut()
            Dim query As String = "SELECT books.Book_title, checkout.ID FROM (books INNER JOIN checkout ON books.Book_id = checkout.bookId) WHERE checkout.custId = @CustID AND checkout.Date_Returned IS NULL"
            Try
                ConnectDatabase()
                Dim dataAdapter = New MySqlDataAdapter(query, msdConnection.connection)
                Dim dataTable = New DataTable
                dataAdapter.SelectCommand.Parameters.AddWithValue("@CustID", CheckinCustomerCombobox.SelectedValue)
                dataAdapter.Fill(dataTable)
                CheckinBookCombobox.DataSource = dataTable
                CheckinBookCombobox.DisplayMember = "Book_title"
                CheckinBookCombobox.ValueMember = "ID"
                StatusLabel.Text = "Books out by selected customer loaded."
            Catch ex As Exception
                Console.WriteLine("Query failed: " + ex.ToString)
                StatusLabel.Text = "Database connection error."
            Finally
                DisconnectDatabase()
            End Try
        End Sub

        'Subroutine called when checkin button is clicked in the checkin panel.
        Private Sub CheckinButton_click(ByVal sender As Object, ByVal e As EventArgs) Handles CheckinButton.Click
            Dim query As String = "UPDATE checkout SET Date_Returned = CURDATE() WHERE ID = @ID"
            Using command As New MySqlCommand(query, msdConnection.connection)
                Try
                    ConnectDatabase()
                    command.Parameters.AddWithValue("@ID", CheckinBookCombobox.SelectedValue)
                    command.Prepare()
                    command.ExecuteNonQuery()
                    LoadBooksCheckedOut()
                    StatusLabel.Text = "Book checked in."
                Catch ex As Exception
                    Console.WriteLine("Query failed: " + ex.ToString)
                    StatusLabel.Text = "Database connection error."
                Finally
                    DisconnectDatabase()
                End Try
            End Using
        End Sub

        'Subroutine called when close button is clicked in checkin panel.
        Private Sub CheckinCloseButton_click(ByVal sender As Object, ByVal e As EventArgs) Handles CheckinCloseButton.Click
            CheckinPanel.Visible = False
        End Sub

        'Subroutine called when close button is clicked in view customer panel. Hides the panel.
        Private Sub ViewCustomerCloseButton_click(ByVal sender As Object, ByVal e As EventArgs) Handles ViewCustomerCloseButton.Click
            ViewCustomerPanel.Visible = False
        End Sub

        'Subroutine called when add customer button is clicked. Inserts a row into the database that adds a new customer with information read from controls.
        Private Sub AddCustomerButton_click(ByVal sender As Object, ByVal e As EventArgs) Handles AddCustomerButton.Click
            Dim query As String = "INSERT INTO customer(first_name, last_name, dob, teacher) VALUES (@First,@Last,@Dob,@IsTeacher)"
            Using command As New MySqlCommand(query, msdConnection.connection)
                Try
                    ConnectDatabase()
                    command.Parameters.AddWithValue("@First", NewFirstNameTextbox.Text)
                    command.Parameters.AddWithValue("@Last", NewLastNameTextbox.Text)
                    command.Parameters.AddWithValue("@Dob", NewCustomerDateTimePicker.Value.Date)
                    command.Parameters.AddWithValue("@IsTeacher", NewCustomerCheckbox.Checked)
                    command.Prepare()
                    command.ExecuteNonQuery()
                    StatusLabel.Text = "Customer added."
                Catch ex As Exception
                    Console.WriteLine("Query Failed" + ex.ToString)
                    StatusLabel.Text = "Database connection error."
                Finally
                    DisconnectDatabase()
                End Try
            End Using
        End Sub

        'Subroutine called when close button is clicked. Hides panel.
        Private Sub CloseNewCustomerPanelButton_click(ByVal sender As Object, ByVal e As EventArgs) Handles CloseNewCustomerPanelButton.Click
            NewCustomerPanel.Visible = False
        End Sub

        'Subroutine called when selecting customer ids in the combobox. Reads existing customer data from the database.
        Private Sub UpdateCustomerIdCombobox_click(ByVal sender As Object, ByVal e As EventArgs) Handles UpdateCustomerIdCombobox.SelectedValueChanged
            Dim query As String = "SELECT * FROM customer WHERE customerId = @ID"
            Using command As New MySqlCommand(query, msdConnection.connection)
                Try
                    ConnectDatabase()
                    command.Parameters.AddWithValue("@ID", UpdateCustomerIdCombobox.SelectedValue)
                    command.Prepare()
                    Dim dataReader As MySqlDataReader = command.ExecuteReader
                    While dataReader.Read
                        UpdateFirstNameTextbox.Text = dataReader.GetString(1)
                        UpdateLastNameTextbox.Text = dataReader.GetString(2)
                        UpdateCustomerDateTimePicker.Value = dataReader.GetDateTime(3)
                        UpdateCustomerCheckbox.Checked = dataReader.GetBoolean(4)
                    End While
                    StatusLabel.Text = "Customer data loaded."
                Catch ex As Exception
                    Console.WriteLine(ex.ToString)
                    StatusLabel.Text = "Database connection error."
                Finally
                    DisconnectDatabase()
                End Try
            End Using
        End Sub

        'Subroutine called when update customer button is clicked. Launches query to update row in database.
        Private Sub UpdateCustomerButton_click(ByVal sender As Object, ByVal e As EventArgs) Handles UpdateCustomerButton.Click
            Dim query As String = "UPDATE customer SET first_name=@First, last_name=@Last, dob=@Dob, teacher=@IsTeacher WHERE customerId = @Id"
            Using command As New MySqlCommand(query, msdConnection.connection)
                Try
                    ConnectDatabase()
                    command.Parameters.AddWithValue("@Id", UpdateCustomerIdCombobox.SelectedValue)
                    command.Parameters.AddWithValue("@First", UpdateFirstNameTextbox.Text)
                    command.Parameters.AddWithValue("@Last", UpdateLastNameTextbox.Text)
                    command.Parameters.AddWithValue("@Dob", UpdateCustomerDateTimePicker.Value.Date)
                    command.Parameters.AddWithValue("@IsTeacher", UpdateCustomerCheckbox.Checked)
                    command.Prepare()
                    command.ExecuteNonQuery()
                    StatusLabel.Text = "Customer updated."
                Catch ex As Exception
                    Console.WriteLine("Query failed: " + ex.ToString)
                    StatusLabel.Text = "Database connection error."
                Finally
                    DisconnectDatabase()
                End Try
            End Using
        End Sub

        'Subroutine called when close button is clicked. Hides panel.
        Private Sub CloseUpdateCustomerPanelButton_click(ByVal sender As Object, ByVal e As EventArgs) Handles CloseUpdateCustomerPanelButton.Click
            UpdateCustomerPanel.Visible = False
        End Sub

        'Subroutine called when close button is clicked. Hides panel.
        Private Sub AllBooksCheckedOutCloseButton_click(ByVal sender As Object, ByVal e As EventArgs) Handles AllBooksCheckedOutCloseButton.Click
            AllBooksCheckedOutPanel.Visible = False
        End Sub

        'Subroutine callled when generating books checked out by customer. Reads the customer names, book title and author that are out for a specified customer.
        Private Sub BooksCheckedOutByCustomerGenerateButton_click(ByVal sender As Object, ByVal e As EventArgs) Handles BooksCheckedOutByCustomerGenerateButton.Click
            Dim query As String = "SELECT customer.first_name, customer.last_name, customer.customerId, books.Book_title, books.Author, checkout.Date_Due_Back FROM ((checkout INNER JOIN customer on checkout.custId = customer.customerId) INNER JOIN books on checkout.bookId = books.Book_id) WHERE customer.customerId = @ID && checkout.Date_Returned IS NULL"
            Dim dateDiffQuery As String = "SELECT DATEDIFF(checkout.date_due_back, CURDATE()) FROM (checkout INNER JOIN customer ON checkout.custId = customer.customerId) WHERE checkout.custId = @ID AND checkout.date_returned IS NULL"
            Dim custId As Int32 = Convert.ToInt32(BooksCheckedOutByCustomerCombobox.SelectedValue)
            Dim dataTable As New DataTable
            Using dataAdapter As New MySqlDataAdapter(query, msdConnection.connection)
                Try
                    ConnectDatabase()
                    dataAdapter.SelectCommand.Parameters.AddWithValue("@ID", BooksCheckedOutByCustomerCombobox.SelectedValue)
                    dataAdapter.Fill(dataTable)
                    dataTable.Columns.Add("Days early/late", GetType(String))
                    Dim dateDiffCommand As New MySqlCommand(dateDiffQuery, msdConnection.connection)
                    dateDiffCommand.Parameters.AddWithValue("@ID", BooksCheckedOutByCustomerCombobox.SelectedValue)
                    dateDiffCommand.Prepare()

                    Dim dataReader As MySqlDataReader = dateDiffCommand.ExecuteReader()
                    For i = 0 To (dataTable.Rows.Count() - 1) Step 1
                        dataReader.Read()
                        Dim days As Int32 = dataReader.GetInt32(0)
                        If days < 0 Then
                            dataTable.Rows(i)("Days early/late") = Math.Abs(days).ToString + " days late."
                        Else
                            dataTable.Rows(i)("Days early/late") = days.ToString + " days early."
                        End If
                    Next
                    BooksCheckedOutByCustomerDataSource.DataSource = dataTable
                Catch ex As Exception
                    Console.WriteLine("Query failed: " + ex.ToString)
                Finally
                    DisconnectDatabase()
                End Try
            End Using
        End Sub

        'Subroutine called when close button is clicked. Hides the panel.
        Private Sub BooksCheckedOutByCustomerCloseButton_click(ByVal sender As Object, ByVal e As EventArgs) Handles BooksCheckedOutByCustomerCloseButton.Click
            BooksCheckedOutByCustomerPanel.Visible = False
        End Sub

        'Subroutine called when close button is clicked in late books report panel. Hides the panel.
        Private Sub LateBooksReportCloseButton_click(ByVal sender As Object, ByVal e As EventArgs) Handles LateBooksReportCloseButton.Click
            LateBooksReportPanel.Visible = False
        End Sub

        'Subroutine called when update config combobox is clicked. Reads current config values from database.
        Private Sub UpdateConfigCombobox_click(ByVal sender As Object, ByVal e As EventArgs) Handles UpdateConfigCombobox.SelectedValueChanged
            Dim query As String = "SELECT Numbooks, Latefee FROM config WHERE ConfigID = @ID"
            Using command As New MySqlCommand(query, msdConnection.connection)
                Try
                    ConnectDatabase()
                    command.Parameters.AddWithValue("@ID", UpdateConfigCombobox.SelectedValue)
                    command.Prepare()
                    Dim dataReader As MySqlDataReader = command.ExecuteReader
                    While dataReader.Read
                        UpdateConfigCheckoutTextbox.Text = dataReader.GetString(0)
                        UpdateConfigLateFeeTextbox.Text = dataReader.GetString(1)
                    End While
                    StatusLabel.Text = "Config data loaded."
                Catch ex As Exception
                    Console.WriteLine(ex.ToString)
                    StatusLabel.Text = "Database connection error."
                Finally
                    DisconnectDatabase()
                End Try
            End Using
        End Sub

        'Subroutine called when update checkout button is clicked.
        Private Sub UpdateConfigCheckoutButton_click(ByVal sender As Object, ByVal e As EventArgs) Handles UpdateConfigCheckoutButton.Click
            Dim query = "UPDATE config SET Numbooks = @NumBooks WHERE ConfigID = @Role"
            Using command As New MySqlCommand(query, msdConnection.connection)
                Try
                    ConnectDatabase()
                    command.Parameters.AddWithValue("@NumBooks", UpdateConfigCheckoutTextbox.Text)
                    command.Parameters.AddWithValue("@Role", UpdateConfigCombobox.SelectedValue)
                    command.Prepare()
                    command.ExecuteNonQuery()
                    StatusLabel.Text = "Number of checkouts allowed updated."
                Catch ex As Exception
                    Console.WriteLine("Query failed: " + ex.ToString)
                    StatusLabel.Text = "Database connection error."
                Finally
                    DisconnectDatabase()
                End Try
            End Using
        End Sub

        'Subroutine called when update late fee is clicked.
        Private Sub UpdateConfigLateFeeButton_click(ByVal sender As Object, ByVal e As EventArgs) Handles UpdateConfigLateFeeButton.Click
            Dim query = "UPDATE config SET Latefee = @LateFee WHERE ConfigID = @Role"
            Using command As New MySqlCommand(query, msdConnection.connection)
                Try
                    ConnectDatabase()
                    command.Parameters.AddWithValue("@LateFee", UpdateConfigLateFeeTextbox.Text)
                    command.Parameters.AddWithValue("@Role", UpdateConfigCombobox.SelectedValue)
                    command.Prepare()
                    command.ExecuteNonQuery()
                    StatusLabel.Text = "Late fee updated."
                Catch ex As Exception
                    Console.WriteLine("Query failed: " + ex.ToString)
                    StatusLabel.Text = "Database connection error."
                Finally
                    DisconnectDatabase()
                End Try
            End Using
        End Sub

        'Subroutine called when close button is clicked. Hides the panel.
        Private Sub UpdateConfigCloseButton_click(ByVal sender As Object, ByVal e As EventArgs) Handles UpdateConfigCloseButton.Click
            UpdateConfigPanel.Visible = False
        End Sub
    End Class

    'Main sub called at app start. Runs new instance of our form.
    Public Sub Main()
        Application.EnableVisualStyles()
        Application.Run(New Form1)
    End Sub
End Module

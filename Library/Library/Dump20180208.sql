-- MySQL dump 10.13  Distrib 5.7.21, for Linux (x86_64)
--
-- Host: localhost    Database: library
-- ------------------------------------------------------
-- Server version	5.7.21

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `address`
--

DROP TABLE IF EXISTS `address`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `address` (
  `AddrId` int(11) NOT NULL AUTO_INCREMENT,
  `Addr1` varchar(255) DEFAULT NULL,
  `Addr2` varchar(255) DEFAULT NULL,
  `ZIP` int(5) DEFAULT NULL,
  `AddrCustId` int(11) DEFAULT NULL,
  PRIMARY KEY (`AddrId`),
  KEY `AddressCustomerFK` (`AddrCustId`),
  CONSTRAINT `AddressCustomerFK` FOREIGN KEY (`AddrCustId`) REFERENCES `customer` (`customerId`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `address`
--

LOCK TABLES `address` WRITE;
/*!40000 ALTER TABLE `address` DISABLE KEYS */;
INSERT INTO `address` VALUES (1,'4753 Carpenter Trail',NULL,24252,1),(2,'2 Ruskin Circle',NULL,24251,2),(3,'91 Erie Point',NULL,24254,3),(4,'5508 Lighthouse Bay Place',NULL,24232,4),(5,'83233 Anniversary Parkway',NULL,24237,5),(6,'65752 Algoma Trail',NULL,24243,6),(7,'57017 Norway Maple Crossing',NULL,24271,7),(8,'3291 Graedel Crossing',NULL,24242,8),(9,'973 Twin Pines Trail',NULL,24236,9),(10,'3374 Anniversary Point',NULL,24279,10),(11,'16 Hoard Terrace',NULL,24229,11),(12,'967 Gerald Street',NULL,24227,12),(13,'372 Marquette Trail',NULL,24228,13),(14,'046 Eliot Junction',NULL,24262,14),(15,'07 Sachs Point',NULL,24269,15),(16,'8666 Maryland Center',NULL,24259,16),(17,'5120 Brown Court',NULL,24224,17),(18,'26245 Grayhawk Way',NULL,24252,18),(19,'9835 Rusk Street',NULL,24238,19),(20,'3320 Crownhardt Way',NULL,24277,20),(21,'4839 Mandrake Way',NULL,24263,21),(22,'40272 Canary Drive',NULL,24235,22),(23,'53058 Lakewood Trail',NULL,24275,23),(24,'71 Sherman Center',NULL,24226,24),(25,'50 Clemons Park',NULL,24232,25),(26,'69 Hudson Junction',NULL,24225,26),(27,'82075 Del Mar Court',NULL,24233,27),(28,'667 Shopko Crossing',NULL,24272,28),(29,'4200 Merry Point',NULL,24276,29),(30,'1404 Fuller Terrace',NULL,24243,30),(31,'325 Clarendon Junction',NULL,24246,31),(32,'41349 Eagan Drive',NULL,24229,32),(33,'5384 Carey Plaza',NULL,24233,33),(34,'6 1st Street',NULL,24250,34),(35,'2821 Carioca Center',NULL,24249,35),(36,'53784 Bashford Crossing',NULL,24259,36),(37,'5 School Street',NULL,24276,37),(38,'71 Superior Terrace',NULL,24263,38),(39,'52853 Chinook Point',NULL,24249,39),(40,'80 Fuller Street',NULL,24251,40),(41,'8 Dapin Road',NULL,24250,41),(42,'8 Valley Edge Parkway',NULL,24252,42),(43,'2329 Coleman Drive',NULL,24259,43),(44,'64 Loeprich Trail',NULL,24278,44),(45,'68 Autumn Leaf Drive',NULL,24262,45),(46,'8875 Columbus Parkway',NULL,24264,46),(47,'3 Namekagon Plaza',NULL,24257,47),(48,'33 Anzinger Hill',NULL,24230,48),(49,'0 Hagan Circle',NULL,24226,49),(50,'6 Gale Hill',NULL,24239,50),(51,'8 Bay Drive',NULL,24243,51),(52,'3 Crownhardt Circle',NULL,24256,52),(53,'2 Jay Park',NULL,24266,53),(54,'051 Independence Place',NULL,24228,54),(55,'0135 Macpherson Point',NULL,24274,55),(56,'2 Paget Trail','64109 CEDEX',24244,56),(57,'1 Heath Place',NULL,24229,57),(58,'74620 Dakota Crossing',NULL,24236,58),(59,'521 Harbort Way',NULL,24242,59),(60,'8 School Hill',NULL,24252,60),(61,'5 Mayer Circle',NULL,24226,61),(62,'735 Golf View Plaza',NULL,24245,62),(63,'19838 Mandrake Plaza',NULL,24227,63),(64,'7 Summerview Center',NULL,24243,64),(65,'88 Judy Road',NULL,24227,65),(66,'7907 Porter Plaza',NULL,24252,66),(67,'63 Havey Point',NULL,24259,67),(68,'578 Twin Pines Place',NULL,24225,68),(69,'115 Russell Court',NULL,24248,69),(70,'63779 Manley Junction',NULL,24244,70),(71,'559 Pearson Junction',NULL,24250,71),(72,'87 Cambridge Lane',NULL,24256,72),(73,'4440 Butterfield Trail',NULL,24224,73),(74,'386 Straubel Park',NULL,24265,74),(75,'1865 Pawling Park',NULL,24226,75),(76,'6560 Center Street',NULL,24231,76),(77,'8485 Stoughton Hill',NULL,24235,77),(78,'25983 Dahle Hill',NULL,24231,78),(79,'71597 Lakeland Terrace',NULL,24256,79),(80,'2004 Red Cloud Trail',NULL,24262,80),(81,'5060 Sherman Plaza',NULL,24259,81),(82,'7 Mayer Terrace',NULL,24242,82),(83,'5 Schurz Plaza',NULL,24246,83),(84,'90441 Melrose Street',NULL,24263,84),(85,'71 Clarendon Trail',NULL,24274,85),(86,'2 Hoard Point',NULL,24248,86),(87,'13178 Glendale Court',NULL,24233,87),(88,'0 Rockefeller Trail',NULL,24264,88),(89,'03797 Artisan Road',NULL,24280,89),(90,'4 Evergreen Street',NULL,24232,90),(91,'067 Sage Street',NULL,24280,91),(92,'009 Meadow Vale Road',NULL,24235,92),(93,'231 Morrow Circle',NULL,24231,93),(94,'8895 Sommers Point',NULL,24230,94),(95,'19 East Pass',NULL,24254,95),(96,'578 Merrick Street',NULL,24252,96),(97,'08 Anhalt Street',NULL,24230,97),(98,'97 Vermont Circle',NULL,24232,98),(99,'062 Novick Avenue',NULL,24230,99),(100,'391 New Castle Circle',NULL,24235,100);
/*!40000 ALTER TABLE `address` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `books`
--

DROP TABLE IF EXISTS `books`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `books` (
  `Book_Id` varchar(15) NOT NULL,
  `Book_title` varchar(20) NOT NULL,
  `Author` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`Book_Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `books`
--

LOCK TABLES `books` WRITE;
/*!40000 ALTER TABLE `books` DISABLE KEYS */;
INSERT INTO `books` VALUES ('040RBF7B5M','IACULIS','Garnette Dallewater'),('2KTV0X67BB','EST ET','Anatollo Chominski'),('2UBJ5PV5C8','ODIO ODIO','Atlanta Giannazzi'),('4OAS3ZJFJ9','HAC HABITASSE','Cordey Gannicleff'),('4UDGP9CTU4','RHONCUS DUI','Rees Scriver'),('5KJ1B9SF08','LECTUS','Ellerey Follan'),('679IAD75G2','DUIS','Skelly Semmence'),('6AB1G691E8','IPSUM INTEGER','Ulrike Dryden'),('6AS897NPLR','IN','Conrad Smeeth'),('7377R59FAC','NULLA','Annice Seefeldt'),('7H4KNRZ8GJ','CONSECTETUER','Lorene Desvignes'),('81VFOKG4UW','VIVAMUS VEL','Guglielmo Glyne'),('8UVTVE1FX0','COMMODO VULPUTATE','Debbie Gliddon'),('9TVO1VHLFR','METUS','Inigo Shergill'),('A2B3PXBLKE','LUCTUS','Rafi Samuels'),('AJCL6ZIID1','VEL','Ferdy Posnett'),('AN3MKSFF4N','PORTTITOR','Mariellen Dincey'),('BGSIZ80XBS','NULLAM ORCI','Mozelle Fiorentino'),('E5N5M7XAE9','INTEGER PEDE','Lindi Odlin'),('EAG9AC7KD6','IPSUM','Mose Calender'),('EGFIO5YGPC','PULVINAR','Emalia Parrington'),('EOYYMJSU04','NULLAM SIT','Ludovika Alyokhin'),('F6NNQ2QOW4','EGET SEMPER','Ciro Cousens'),('FD9ZIQZF2A','ODIO','Con Atrill'),('I06KHL003Z','A ODIO','Tito Grantham'),('I6F74C11US','PROIN','Nicolea Smallpeace'),('J0LR08UUVI','SOLLICITUDIN','Rivalee Scarre'),('JFYLC6DYY1','TURPIS','Josephina Rowesby'),('JRRPHILK19','ORCI EGET','Danette Tincey'),('K29SRJ3T4K','ANTE','Mendy Witchalls'),('KBWWZOL9QH','NATOQUE PENATIBUS','Jasmina Rubinsaft'),('KVKQTQ3T3O','IN LACUS','Whittaker Paddeley'),('M7UV0O8OBY','AT DIAM','Peri Stockdale'),('NVLQOGQLUY','AT','Lela Keam'),('O0ZRU6VN7G','METUS','Curry Gabler'),('OZQBWCDE96','ALIQUAM AUGUE','Dannie Snell'),('Q4Y3QBWIWH','TRISTIQUE','Artemus Rosencrantz'),('QZ3WV7VC8J','MUS','Barrett McGlynn'),('RYBQPRA3XO','NULLA','Alex Elloway'),('SBL1VR0RE3','TORTOR','Olympia Densie'),('UGO5QO1N11','QUISQUE','Torr Prene'),('UKVCVYFSXK','IN','Emelyne Kelcher'),('UQ5WFNJ070','LOREM QUISQUE','Maxine Shillinglaw'),('UVH15I9JV8','LOREM','Cyril Monroe'),('UZXQ9G79WL','ELIT','Tracee Vasilik'),('VZHQXG90IN','CONSEQUAT NULLA','Sile Wivell'),('W1P0TCB13R','TURPIS ELEMENTUM','Arturo Cruz'),('WM9STFGYQT','PEDE','Isobel Trusty'),('XTCKN5UDLM','LIGULA','Clemmy Bisacre'),('Y0KKKFRVLZ','EU','Yankee Sherston');
/*!40000 ALTER TABLE `books` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `checkout`
--

DROP TABLE IF EXISTS `checkout`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `checkout` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Date_Borrowed` date NOT NULL,
  `Date_Due_Back` date NOT NULL,
  `Date_Returned` date DEFAULT NULL,
  `custId` int(11) NOT NULL,
  `bookId` varchar(15) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `custcheck` (`custId`),
  KEY `id_book` (`bookId`),
  CONSTRAINT `custcheck` FOREIGN KEY (`custId`) REFERENCES `customer` (`customerId`),
  CONSTRAINT `id_book` FOREIGN KEY (`bookId`) REFERENCES `books` (`Book_Id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `checkout`
--

LOCK TABLES `checkout` WRITE;
/*!40000 ALTER TABLE `checkout` DISABLE KEYS */;
INSERT INTO `checkout` VALUES (1,'2018-01-08','2018-01-15',NULL,1,'040RBF7B5M'),(2,'2018-01-09','2018-01-16','2018-01-15',1,'2KTV0X67BB'),(3,'2018-01-14','2018-01-21',NULL,2,'2UBJ5PV5C8'),(4,'2018-01-08','2018-01-15',NULL,2,'4OAS3ZJFJ9'),(5,'2018-01-08','2018-01-15',NULL,3,'4UDGP9CTU4'),(6,'2018-01-08','2018-01-15',NULL,4,'5KJ1B9SF08'),(7,'2018-01-08','2018-01-15',NULL,5,'679IAD75G2'),(12,'2018-01-19','2018-01-26',NULL,9,'81VFOKG4UW');
/*!40000 ALTER TABLE `checkout` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `config`
--

DROP TABLE IF EXISTS `config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `config` (
  `ConfigID` int(11) NOT NULL AUTO_INCREMENT,
  `CategID` tinyint(1) DEFAULT NULL,
  `Numbooks` int(11) DEFAULT NULL,
  `Numdays` int(11) DEFAULT NULL,
  `Latefee` decimal(5,2) DEFAULT NULL,
  PRIMARY KEY (`ConfigID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `config`
--

LOCK TABLES `config` WRITE;
/*!40000 ALTER TABLE `config` DISABLE KEYS */;
INSERT INTO `config` VALUES (1,0,2,7,2.00),(2,1,5,14,4.00);
/*!40000 ALTER TABLE `config` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer`
--

DROP TABLE IF EXISTS `customer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer` (
  `customerId` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `dob` date DEFAULT NULL,
  `teacher` tinyint(1) NOT NULL,
  PRIMARY KEY (`customerId`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer`
--

LOCK TABLES `customer` WRITE;
/*!40000 ALTER TABLE `customer` DISABLE KEYS */;
INSERT INTO `customer` VALUES (1,'Jeralee','Duchesne','2000-02-29',0),(2,'Farlay','Emerson','2000-08-29',0),(3,'Cassy','Halfhyde','2001-08-28',0),(4,'Shayne','Annear','2009-08-23',0),(5,'Byram','Karim','2000-09-25',0),(6,'Stephie','Hoff','2007-11-25',0),(7,'Maxy','Monument','2005-08-11',0),(8,'Gigi','Slatford','2006-12-06',0),(9,'Andeee','Verni','2005-08-05',0),(10,'Briny','Crain','2003-03-07',0),(11,'Griffie','Sallarie','2006-10-31',0),(12,'Jessey','Dealtry','2002-03-09',0),(13,'Atlante','Cheesman','2009-09-08',0),(14,'Ivette','Hattrick','2001-07-07',0),(15,'Leoine','O\'Fihillie','2004-07-11',0),(16,'Rosamund','Brockie','2006-06-10',0),(17,'Germaine','Garmston','2005-08-12',0),(18,'Marjorie','Swann','2008-02-26',0),(19,'Corinna','Peal','2003-07-17',0),(20,'Merry','Gillcrist','2003-06-21',0),(21,'Teddi','Stannislawski','2000-04-03',0),(22,'Mahalia','Reape','2003-02-13',0),(23,'Katinka','Pidduck','2005-08-27',0),(24,'Karoly','Durward','2000-12-19',0),(25,'Cecilius','Marjanski','2008-08-22',0),(26,'Vern','Babington','2003-12-29',0),(27,'Natale','Schoales','2006-11-09',0),(28,'Mercedes','Draysay','2006-03-26',0),(29,'Tawnya','Judron','2006-11-28',0),(30,'Janeta','Vereker','2006-10-05',0),(31,'Berkley','Moller','2005-03-26',0),(32,'Evyn','Woolward','2003-09-10',0),(33,'Natividad','Malden','2007-02-11',0),(34,'Trent','Stinson','2008-03-10',0),(35,'Tracey','Housen','2003-08-18',0),(36,'Zacharia','Kimmitt','2002-09-12',0),(37,'Consuela','Peachment','2007-09-30',0),(38,'Johann','Theze','2008-03-07',0),(39,'Lari','Dukes','2009-03-29',0),(40,'Patsy','Truluck','2004-01-20',0),(41,'Bianca','Plan','2003-07-30',0),(42,'Rock','Draye','2008-11-15',0),(43,'Eliza','Condy','2001-10-28',0),(44,'Annaliese','Corwood','2008-08-17',0),(45,'Chelsae','Tixall','2008-05-06',0),(46,'Lovell','Bennedsen','2001-06-05',0),(47,'Gertruda','Errichelli','2009-06-09',0),(48,'Andriana','Jacombs','2006-08-22',0),(49,'Baillie','Laguerre','2000-08-26',0),(50,'Rudolf','Capeling','2002-05-19',0),(51,'Abbi','Woodley','1990-02-20',1),(52,'Nola','de Nore','1978-11-29',1),(53,'Olva','Whiteside','1979-03-18',1),(54,'Bonny','Westberg','1989-11-21',1),(55,'Austin','Muncaster','1989-08-30',1),(56,'Bronny','Itzcovich','1988-11-05',1),(57,'Sophie','Huffa','1976-05-01',1),(58,'Perri','Fifoot','1993-10-25',1),(59,'Belvia','Siaspinski','1995-05-25',1),(60,'Belinda','Brandes','1980-12-12',1),(61,'Elisha','Geer','1991-11-22',1),(62,'Ernesta','Reside','1976-08-25',1),(63,'Zita','Patterfield','1993-05-26',1),(64,'Darcie','Gebhardt','1988-03-22',1),(65,'Georgena','Bayles','1983-07-20',1),(66,'Dory','Petegree','1987-08-13',1),(67,'Blair','Lenahan','1974-06-15',1),(68,'Marline','Mixon','1989-11-05',1),(69,'Judy','Seward','1979-02-18',1),(70,'Tanhya','Umfrey','1978-12-03',1),(71,'Everard','Copping','1974-08-06',1),(72,'Moshe','Moscone','1976-11-26',1),(73,'Vanna','Spillane','1985-02-09',1),(74,'Saunders','Holstein','1988-10-08',1),(75,'Ashley','Clifft','1981-05-09',1),(76,'Dominique','Brownsey','1977-08-02',1),(77,'Marcel','Maylour','1988-07-31',1),(78,'Zorine','De La Haye','1981-11-20',1),(79,'Christoffer','Ellerton','1983-04-26',1),(80,'Nobe','Pietrowicz','1974-06-05',1),(81,'Daven','Sokell','1971-05-12',1),(82,'Janela','Mattersley','1996-03-11',1),(83,'Lisette','Dabell','1976-05-31',1),(84,'Sibylle','Hugo','1975-07-16',1),(85,'Hersh','Thake','1972-03-26',1),(86,'Bendicty','Breukelman','1970-05-04',1),(87,'Danna','Francescotti','1996-01-23',1),(88,'Tiebout','Simione','1970-11-27',1),(89,'Janet','Fyndon','1977-03-16',1),(90,'Byrann','Duhig','1990-01-26',1),(91,'Carlotta','Kelk','1971-03-09',1),(92,'Valentia','Winny','1972-12-09',1),(93,'Danie','Cash','1983-08-17',1),(94,'Drusi','Pippin','1971-11-08',1),(95,'Jeffrey','Ollarenshaw','1980-12-27',1),(96,'Kimmi','Souttar','1985-12-11',1),(97,'Valencia','Orrice','1978-01-27',1),(98,'Caria','Billborough','1980-10-25',1),(99,'Gabrielle','Buckthought','1973-08-13',1),(100,'Catarina','McGrudder','1974-02-12',1);
/*!40000 ALTER TABLE `customer` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-02-09  0:07:14

﻿Imports MySql.Data.MySqlClient
Imports System.IO

Module msdConnection
    Public connection As New MySqlConnection
    Private connectionString As String = ""

    Public Sub BuildConnectionString()
        Try
            Using sr As New StreamReader("database.config")
                Dim line As String
                Do While sr.Peek() >= 0
                    line = sr.ReadLine
                    connectionString += line
                Loop
            End Using
            'Console.WriteLine(connectionString)
        Catch ex As Exception
            Console.WriteLine(ex.ToString)
        End Try
    End Sub

    Public Sub ConnectDatabase()
        Try
            If connection.State = ConnectionState.Closed Then
                connection.ConnectionString = connectionString
                connection.Open()
                Console.WriteLine(connection.State.ToString)
            End If
        Catch ex As Exception
            Console.WriteLine(ex.ToString)
        End Try
    End Sub

    Public Sub DisconnectDatabase()
        Try
            connection.Close()
            Console.WriteLine(connection.State.ToString)
        Catch ex As Exception
            Console.WriteLine(ex.ToString)
        End Try
    End Sub
End Module
